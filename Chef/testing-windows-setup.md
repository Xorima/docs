Windows Testing Setup
===

Testing on windows has a couple small issues, mainly around licensing, to resolve these follow the steps found here:
https://learn.chef.io/modules/local-development/windows/virtualbox/get-set-up#/

which can be summaried as:

```
git clone https://github.com/mwrock/packer-templates.git
packer build -force -only virtualbox-iso vbox-2012r2.json
ls windows2012r2min-virtualbox.box
vagrant box add windows-2012r2 windows2012r2min-virtualbox.box
vagrant box list
vagrant plugin install vagrant-winrm

```
this will get you a fully setup and working windows testing environment