Cookbooks
===

## What are they
Cookbooks are chef's runlist, they list out the desired state of the system in an order and chef ensures it is in that state and makes any changes in the same order

### Create a cookbook

To create a cookbook run the following:

```
chef generate cookbook <name>
```

### Resources
A Resource is a statement of configuration policy that desribes the desired state of the system in a human readable format with actions that need to be applied. eg:

```
directory '/tmp/folder' do
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end
```