Chef
===

### Overview
Chef is a configuration management tool written in Ruby. It uses a centalised server with agents model, where the agents call the server and then resolve any difference between the desired state and the current state

## Directory Setup

A standard directory tree should look like this:

```
chef/
├── cookbooks
└── roles
```

## Cookbooks

### What are they?
They are the way that chef orginises its version of "Playbooks" these are run lists of desired states of the system. 


### How to create them?

ensure you have a cookbooks repo
```
mkdir cookbooks
cd cookbooks
```

To create a cookbook you will need to run

```
chef generate cookbook <path/name>
```
## Testing

Chef has the ability to do TDD with a lot of different providers from VirtualBox to AWS. This allows you to do TDD no matter what the setup you have

I recommend using virtualbox for quick local itterations however. 

More is available inside [Testing](../Chef/testing.md)

### Methods
