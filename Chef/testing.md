Testing 
===

Chef has multiple different types of tests, integration tests (InSpec), UnitTesting (ChefSpec) and lint testing (Cookstyle / FoodCritic) 

### Why test
Why not? Give me a good reason not to write scalable code that you know automatically will always just work without days or manual testing. 

## InSpec Integration Testing

The tests are designed to be easily readable and resemble natural language, here is an example:
```
describe '<entity>' do
  it { <expection> }
end
```

The test file is automatically created when you generate a cookbook, it is located: test\smoke\default\default.rb

Test syntax and reference can be found here: http://inspec.io/docs/reference/resources/

Place youe tests inside of this file and then you can use kitchen to run the tests

### Kitchen

Kitchen is the main way to run InSpec tests, to run them you need your .kitchen.yml file configured correctly for testing. 

#### Configuration
This section will show you how to configure your kitchen.yml for different providers

##### Vagrant/ Virtualbox
To setup for Vagrant/Virtual box you will need to ensure that the Driver name is  set to "vagrant"

```
---
driver:
  name: vagrant
...
```

### Running 

To run Kitchen tests change into the directory of the cookbook, eg:
```
cd cookbooks/nginx
```

Run kitchen list to verify the instances state, this is normally "Not Created" when first running your tests

```
kitchen list
Instance             Driver   Provisioner  Verifier  Transport  Last Action    Last Error
default-ubuntu-1604  Vagrant  ChefZero     Inspec    Ssh        <Not Created>  <None>
default-centos-7     Vagrant  ChefZero     Inspec    Ssh        <Not Created>  <None>
```

Run either of these commands:

``` 
kitchen converge
```
This will bring up your virtual machine and apply your Chef code

```
kitchen verify
``` 
This will bring up the virtual machine, run your chef code and run your tests. 

Example:

```
kitchen verify
-----> Starting Kitchen (v1.19.2)
-----> Creating <default-ubuntu-1604>...
...


  User root
     ✔  should exist
  System Package
     ∅  nginx should be installed
     expected that `System Package nginx` is installed
  Port 80
     ∅  should be listening
     expected `Port 80.listening?` to return true, got false

Test Summary: 1 successful, 2 failures, 0 skipped
```

To start from fresh run:

```
kitchen destroy
```

To connect to a kitchen test machine run:

```
kitchen login <name>
```
## Testing on Windows

When you want to build or write tests for windows there are some additional considerations that you must think about, the first being how will you connect to the machine, remember by default kitchen uses SSH.

The normal answer is WinRM, in order to configure winrm as your method of communication you will need to add it to your .kitchen.yml file within your recipe like the below:

```
---
driver:
  name: vagrant

provisioner:
  name: chef_zero
  # You may wish to disable always updating cookbooks in CI or other testing environments.
  # For example:
  #   always_update_cookbooks: <%= !ENV['CI'] %>
  always_update_cookbooks: true

verifier:
  name: inspec

transport:
  name: winrm
  elevated: true

platforms:
  - name: windows-2012r2
    driver:
      customize:
        memory: 2048

suites:
  - name: default
    run_list:
      - recipe[wisa::default]
    verifier:
      inspec_tests:
        - test/smoke/default
    attributes:
``` 
This kitchen.yml file works on a Windows Server 2012R2 machine with 2GB or ram assigned to it, you will also notice in the transport section we are connecting with WinRM

After this you should run
```
kitchen list
```

if you get back an error you will need to install the vagrant plugin for winrm using:
```
vagrant plugin install vagrant-winrm
```

kitchen list should then work. 