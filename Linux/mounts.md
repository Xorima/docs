Mounts
======

Systemd:
---

It is possible to mount a file using systemd, this just calls the regular mount commands but I find it nicer to understand

### Creating:

To start with make sure you know the path you are mounting into, in this instance it will be /mnt/docker 
the file needs to be named after this so in our case mnt-docker.mount 


the file should look like this inside:
```
[Unit]
Description=Mount for docker data

[Install]
WantedBy=multi-user.target

[Mount]
What=/dev/vg_docker/docker_volume
Where=/mnt/docker
Type=xfs
Options=defaults
```
Note the Type for file system, it is possible to use this to mount smb shares, nfs or any type of data

once made save in:
/etc/systemd/system

###Executing:

to mount run:
```
systemctl start mnt-docker.mount
```
to check run:
```
systemctl status mnt-docker.mount
```
To set as a startup item run:
```
systemctl enable mnt-docker.mount
```

### Automounting:

Create a file similar to the mount called: mnt-docker.automount

set its contents to:

```
[Unit]
Description=Automount Docker Data

[Automount]
Where=/mnt/docker

[Install]
WantedBy=multi-user.target
```

#### Always automount:

```
systemctl enable mnt-docker.automount
```