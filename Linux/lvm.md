LVM
======

Create:
---

###Partition the drive:

Create an LVM type partition on the drive:
```
Summary Commands:
fdisk /dev/sdb
Command (m for help): n
Select (default p): p
Partition number (1-4, default 1): 1
First sector (2048-41943039, default 2048): <press enter>
Last sector, +sectors or +size{K,M,G} (2048-41943039, default 41943039): <press enter, defaults to largest possible>
Command (m for help): t
Hex code (type L to list all codes): 8e
Command (m for help): s

Full Commands:

[root@centos7 ~]# fdisk /dev/sdb
Welcome to fdisk (util-linux 2.23.2).

Changes will remain in memory only, until you decide to write them.
Be careful before using the write command.

Device does not contain a recognized partition table
Building a new DOS disklabel with disk identifier 0xfd3bf27d.

Command (m for help): n
Partition type:
   p   primary (0 primary, 0 extended, 4 free)
   e   extended
Select (default p): p
Partition number (1-4, default 1): 1
First sector (2048-41943039, default 2048):
Using default value 2048
Last sector, +sectors or +size{K,M,G} (2048-41943039, default 41943039):
Using default value 41943039
Partition 1 of type Linux and of size 20 GiB is set

Command (m for help): p

Disk /dev/sdb: 21.5 GB, 21474836480 bytes, 41943040 sectors
Units = sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk label type: dos
Disk identifier: 0xfd3bf27d

   Device Boot      Start         End      Blocks   Id  System
/dev/sdb1            2048    41943039    20970496   83  Linux

Command (m for help): t
Selected partition 1
Hex code (type L to list all codes): L

 0  Empty           24  NEC DOS         81  Minix / old Lin bf  Solaris
 1  FAT12           27  Hidden NTFS Win 82  Linux swap / So c1  DRDOS/sec (FAT-
 2  XENIX root      39  Plan 9          83  Linux           c4  DRDOS/sec (FAT-
 3  XENIX usr       3c  PartitionMagic  84  OS/2 hidden C:  c6  DRDOS/sec (FAT-
 4  FAT16 <32M      40  Venix 80286     85  Linux extended  c7  Syrinx
 5  Extended        41  PPC PReP Boot   86  NTFS volume set da  Non-FS data
 6  FAT16           42  SFS             87  NTFS volume set db  CP/M / CTOS / .
 7  HPFS/NTFS/exFAT 4d  QNX4.x          88  Linux plaintext de  Dell Utility
 8  AIX             4e  QNX4.x 2nd part 8e  Linux LVM       df  BootIt
 9  AIX bootable    4f  QNX4.x 3rd part 93  Amoeba          e1  DOS access
 a  OS/2 Boot Manag 50  OnTrack DM      94  Amoeba BBT      e3  DOS R/O
 b  W95 FAT32       51  OnTrack DM6 Aux 9f  BSD/OS          e4  SpeedStor
 c  W95 FAT32 (LBA) 52  CP/M            a0  IBM Thinkpad hi eb  BeOS fs
 e  W95 FAT16 (LBA) 53  OnTrack DM6 Aux a5  FreeBSD         ee  GPT
 f  W95 Extd (LBA) 54  OnTrackDM6      a6  OpenBSD         ef  EFI (FAT-12/16/
10  OPUS            55  EZ-Drive        a7  NeXTSTEP        f0  Linux/PA-RISC b
11  Hidden FAT12    56  Golden Bow      a8  Darwin UFS      f1  SpeedStor
12  Compaq diagnost 5c  Priam Edisk     a9  NetBSD          f4  SpeedStor
14  Hidden FAT16 <3 61  SpeedStor       ab  Darwin boot     f2  DOS secondary
16  Hidden FAT16    63  GNU HURD or Sys af  HFS / HFS+      fb  VMware VMFS
17  Hidden HPFS/NTF 64  Novell Netware  b7  BSDI fs         fc  VMware VMKCORE
18  AST SmartSleep  65  Novell Netware  b8  BSDI swap       fd  Linux raid auto
1b  Hidden W95 FAT3 70  DiskSecure Mult bb  Boot Wizard hid fe  LANstep
1c  Hidden W95 FAT3 75  PC/IX           be  Solaris boot    ff  BBT
1e  Hidden W95 FAT1 80  Old Minix
Hex code (type L to list all codes): 8e
Changed type of partition 'Linux' to 'Linux LVM'

Command (m for help): p

Disk /dev/sdb: 21.5 GB, 21474836480 bytes, 41943040 sectors
Units = sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk label type: dos
Disk identifier: 0xfd3bf27d

   Device Boot      Start         End      Blocks   Id  System
/dev/sdb1            2048    41943039    20970496   8e  Linux LVM
Command (m for help): s
```

### Create the physical Volume
```
[root@centos7 ~]# pvcreate /dev/sdb1
  Physical volume "/dev/sdb1" successfully created
```

### Scan to see the difference:
```
[root@centos7 ~]# lvmdiskscan
  /dev/centos/swap [       2.00 GiB]
  /dev/sda1        [     500.00 MiB]
  /dev/centos/root [      27.51 GiB]
  /dev/sda2        [      29.51 GiB] LVM physical volume
  /dev/sdb1        [      20.00 GiB] LVM physical volume
  2 disks
  1 partition
  0 LVM physical volume whole disks
  2 LVM physical volumes

[root@centos7 ~]# pvdisplay
  --- Physical volume ---
  PV Name               /dev/sda2
  VG Name               centos
  PV Size               29.51 GiB / not usable 3.00 MiB
  Allocatable           yes (but full)
  PE Size               4.00 MiB
  Total PE              7554
  Free PE               0
  Allocated PE          7554
  PV UUID               JvDOto-KDiF-gtca-TveX-ne9M-frsB-qsP1aJ

  "/dev/sdb1" is a new physical volume of "20.00 GiB"
  --- NEW Physical volume ---
  PV Name               /dev/sdb1
  VG Name
  PV Size               20.00 GiB
  Allocatable           NO
  PE Size               0
  Total PE              0
  Free PE               0
  Allocated PE          0
  PV UUID               rJ8wl7-xzIN-2qqV-ov7Z-lHKe-ELge-aAV29V

[root@centos7 ~]# pvscan
  PV /dev/sda2   VG centos          lvm2 [29.51 GiB / 0    free]
  PV /dev/sdb1                      lvm2 [20.00 GiB]
  Total: 2 [49.51 GiB] / in use: 1 [29.51 GiB] / in no VG: 1 [20.00 GiB]

```

### Create a volume group:
Creates a volume group, you can list multiple disks here
```
[root@centos7 ~]# vgcreate vg_data /dev/sdb1
  Volume group "vg_data" successfully created
```

### Create a logical Volume:

```
[root@centos7 ~]# lvcreate --name data_volume -l 100%FREE vg_data
  Logical volume "data_volume" created
```

### Format:

I suggest either EXT4 or XFS at the time of writing, bftfs is still in development

```
mkfs.ext4 /dev/vg_data/data_volume
```

Extend:
---
###Rescan the disks:
```
ls /sys/class/scsi_disk/
echo '1' > /sys/class/scsi_disk/0\:0\:0\:0/device/rescan
```

### Extend the physical volume:
```
pvresize /dev/sdb
```

### Extend the logical volume:
This will extend it to take all available disk space
```
lvextend -l +100%FREE /dev/vg_data/data_volume
```

###Resize the file system:
```
resize2fs /dev/vg_data/data_volume
```


Delete:
---
